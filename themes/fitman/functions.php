<?php
/**
 * Include PHP files, nothing else.
 *
 * All the functions, hooks and setup should be on their own
 * filer organized at /inc/. The names of files should describe
 * what the file does as following:
 *
 * `register-`  configures new settings and assets
 * `setup-`     configures existing settings and assets
 * `function-`  adds functions to be used in templates
 *
 * @package itc_starter
 */

/**
 * Configuration
 */
require_once 'inc/_conf/register-assets.php';
require_once 'inc/_conf/register-blocks.php';
require_once 'inc/_conf/register-image-sizes.php';
require_once 'inc/_conf/register-localization.php';
require_once 'inc/_conf/register-menus.php';

/**
 * Editor
 */
require_once 'inc/editor/setup-classic-editor.php';
require_once 'inc/editor/setup-gutenberg.php';
require_once 'inc/editor/setup-theme-support.php';

/**
 * Forms
 */
require_once 'inc/forms/function-search-form.php';

/**
 * Helpers
 */
require_once 'inc/helpers/function-dates.php';
require_once 'inc/helpers/function-entry-footer.php';
require_once 'inc/helpers/function-hardcoded-ids.php';
require_once 'inc/helpers/function-html-attributes.php';
require_once 'inc/helpers/function-last-edited.php';
require_once 'inc/helpers/function-titles.php';
require_once 'inc/helpers/setup-fallbacks.php';

/**
 * Media
 */
require_once 'inc/media/function-image.php';
require_once 'inc/media/function-svg.php';

/**
 * Navigation
 */
require_once 'inc/navigation/function-menu-toggle.php';
require_once 'inc/navigation/function-numeric-posts-nav.php';
require_once 'inc/navigation/function-share-buttons.php';
require_once 'inc/navigation/function-sub-pages-nav.php';
require_once 'inc/navigation/setup-menu-hooks.php';


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ta_tht_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'tht' ),
		'id'            => 'sidebar-right',
		'description'   => 'Main sidebar that appears on the right.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s well">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'ta_tht_widgets_init' );


/**
 * Custom Post Types 
 */
require_once get_template_directory() . '/inc/post-types/CPT.php';
 

/**
 * Testimonials Custom Post Type
 */
require_once get_template_directory() . '/inc/post-types/register-testimonials.php';


/**
 * Locations Custom Post Type
 */
require_once get_template_directory() . '/inc/post-types/register-locations.php';


if( function_exists('acf_add_options_page') ) {
	// Adds ACF Pro options page that used to appear by default in the ACF4 add on
	acf_add_options_page(array(
		'page_title'  => 'Theme panel',
		'menu_title' => 'Theme panel', 
		'menu_slug'  => 'acf-options',
		'capability' => 'edit_posts',
		'redirect'  => false
	));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Header Settings',
		'menu_title' 	=> 'Header',
		'parent_slug' 	=> $parent['header'],
    ));
    
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Contact Settings',
		'menu_title' 	=> 'Contact',
		'parent_slug' 	=> $parent['contact'],
    ));
     
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Social Settings',
		'menu_title' 	=> 'Social',
		'parent_slug' 	=> $parent['social'],
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title' 	=> 'Footer',
		'parent_slug' 	=> $parent['footer'],
    ));

	
  }