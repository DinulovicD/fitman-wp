<?php

/**

 * The template for displaying the footer.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package itc_starter

 */



?>



  </div><!-- #content -->



  <footer style="background-image: url('<?php the_field('footer_contact_background_image', 'option'); ?>'); " id="footer">

    <div class="overlay"></div>

    <div class="left-red">

      <div class="footer-logo">

        <?php if(get_field('footer_logo', 'option')) :?>

          <img src="<?php the_field('footer_logo', 'option'); ?>" alt="" class="img-responsive">

        <?php endif; ?>

      </div>

      <div class="footer-locations">

        <?php if(get_field('footer_location_title', 'option')) :?>

          <h2><?php the_field('footer_location_title', 'option'); ?></h2>

        <?php endif; ?>





        <?php 

            $args = array(  

                'post_type' => 'locations',

                'post_status' => 'publish',

                'posts_per_page' => -1,

                'orderby' => 'date',

                'order' => 'ASC',

            ); 

                            

            $the_query = new WP_Query( $args );

        ?>

        <?php if ( $the_query->have_posts() ) : ?> 

        <div class="slick-carousel">

              <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                  <div class="location-content">

                    <h3>
                        <a href="<?php echo get_field('external_link'); ?>"  target="_blank"><?php the_title(); ?></a>
                    </h3>

                      <ul class="location-content-social">
                        <?php if(get_field('facebook')) :?> 

                          <li><a href="<?php the_field('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i> </a></li>

                        <?php endif; ?>



                        <?php if(get_field('instagram')) :?>

                          <li><a href="<?php the_field('instagram'); ?>" target="_blank"><i class="fa fa-instagram"></i> </a></li>

                        <?php endif; ?>

                      </ul>
                    <div class="week">

                      <?php



                          if( have_rows('work_hours') ):



                          while ( have_rows('work_hours') ) : the_row(); ?>



                                <div class="time-content">

                                  <?php if(get_sub_field('day')) :?>

                                    <span class="day"><?php the_sub_field('day') ?></span>

                                  <?php endif; ?>

                                  <?php if(get_sub_field('time')) :?>

                                    <span class="hours"><?php the_sub_field('time') ?></span>

                                  <?php endif; ?>

                                </div>



                          <?php  endwhile; endif; ?>



                    </div>

                    <a href="<?php echo get_field('external_link'); ?>"  target="_blank" class="btn btn-white-outline">Get started today!</a>

                  </div>

              <?php endwhile;?>

            </div>

          <?php wp_reset_postdata(); ?>

        <?php endif; ?>





      </div>

    </div>

    <div class="container">

      <div class="row">

        <div class="col-lg-6 col-md-12"></div>

        <div class="col-lg-6 col-md-12">



          <div class="contact-form">

            <?php if(get_field('footer_contact_title', 'option')) :?>

              <h2><?php the_field('footer_contact_title', 'option'); ?></h2>

            <?php endif; ?>

            <?php if(get_field('footer_contact_subtitle', 'option')) :?>

              <h1><?php the_field('footer_contact_subtitle', 'option'); ?></h1>

            <?php endif; ?>

           

            <?php if(get_field('footer_contact_form_shortcode', 'option')) :?>

                  <?php echo do_shortcode(get_field('footer_contact_form_shortcode', 'option')); ?> 

            <?php endif; ?>

          </div> 

        </div>

      </div>

    </div>

  </footer>

  <div class="copyright">

    <div class="container">

      <div class="row">

        <div class="col-lg-6 col-md-12"></div>

        <div class="col-lg-6 col-md-12">

          <div class="copyright-content d-flex justify-content-end">

            <div class="left">

            <!-- Footer Privacy & Therms Links ACF Repeater -->

            <?php if( have_rows('footer_pt_links', 'option') ): ?>

            <ul class="social d-flex">

              <?php while( have_rows('footer_pt_links', 'option') ): the_row(); 
  
                $link = get_sub_field('p_and_t', 'option');
                $link_name = get_sub_field('name_link', 'option');
              
                ?>
  
                <li class="slide">
              
                  <?php if( $link ): ?>
                    <a href="<?php echo $link; ?>">
                  <?php endif; ?>
                  
                    <?php echo $link_name; ?>
                  
                  <?php if( $link ): ?>
                    </a>
                  <?php endif; ?>
                  
                    <?php echo $content; ?>
                  
                </li>
                  
              <?php endwhile; ?>
                
            </ul>
                
            <?php endif; ?>

            <!-- Footer Privacy & Therms Links ACF Repeater END -->

              <!-- <ul class="social d-flex">

                <?php if(get_field('facebook', 'option')) :?> 

                    <li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook"></i> </a></li>

                <?php endif; ?>



                <?php if(get_field('instagram', 'option')) :?>

                    <li><a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram"></i> </a></li>

                <?php endif; ?>



                <?php if(get_field('pinterest', 'option')) :?>

                    <li><a href="<?php the_field('pinterest', 'option'); ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>

                <?php endif; ?>



                <?php if(get_field('linkedin', 'option')) :?>

                    <li><a href="<?php the_field('linkedin', 'option'); ?>" target="_blank"><i class="fa fa-linkedin"></i> </a></li>

                <?php endif; ?>

              </ul> -->

              <?php if(get_field('footer_copyright_text', 'option')) :?>

                <p><?php the_field('footer_copyright_text', 'option') ?></p>

              <?php endif; ?>

            </div>

            <div class="right d-flex align-items-center justify-content-center">

              <div class="arrow">

                <a href="" class="scrollup">

                  <i class="fitman-up"></i>

                </a>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>





</div><!-- #page -->



<?php wp_footer(); ?>



</body>

</html>

