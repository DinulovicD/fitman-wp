<?php
/**
 * Template Name: Home
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package itc_starter
 */

get_header(); ?>


<!-- Home -->
<section id="home" style="background-image: url('<?php the_field('header_home_background_image'); ?>'); ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-content">
                    <?php if(get_field('header_home_title_1')) :?>
                        <h2><?php the_field('header_home_title_1'); ?></h2>
                    <?php endif; ?>
                    <?php if(get_field('header_home_title_2')) :?>
                        <h1><?php the_field('header_home_title_2'); ?></h1>
                    <?php endif; ?>
                    <?php if(get_field('header_home_button_name')) :?>
                        <a href="#h-locations" class="btn btn-red"><?php the_field('header_home_button_name'); ?></a>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- Home locations -->
<div id="h-locations">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="owl-carousel owl-locations">
                <?php 
                    $args = array(  
                        'post_type' => 'locations',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'orderby' => 'date',
                        'order' => 'DESC',
                    ); 
                                 
                        $the_query = new WP_Query( $args );
                    ?>
                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <div class="content">
                                    <h3><a href="<?php echo get_field('external_link'); ?>" target="_blank"><?php the_title(); ?></a></h3>
                                    <?php if(get_field('address')) :?>
                                    <p><?php the_field('address'); ?></p>
                                    <?php endif; ?>
                                    <a href="<?php echo get_field('external_link'); ?>" target="_blank" class="btn btn-red">More information</a>
                                    </div>
                                </div>
                
                            <?php endwhile;?>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Home about us -->
<section id="h-about" >
    <div class="container">
        <div class="row"> 
        <div class="col-md-12">
            <div class="content">
                <?php if(get_field('home_about_title')) :?>
                    <h2><?php the_field('home_about_title'); ?></h2>
                <?php endif; ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                    endwhile; endif; ?>
                <div class="padding-helper"></div>
                <?php if(get_field('home_about_button_name')) :?>
                    <a href="<?php the_field('home_about_button_url'); ?>" class="btn btn-black" style="display:none;"><?php the_field('home_about_button_name'); ?></a>
                <?php  endif; ?>
            </div>
        </div>
        </div>
    </div> 
</section>

<!-- Home cta -->
<section id="cta" style="background-image: url('<?php the_field('home_contact_cta_background_image'); ?>'); ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php if(get_field('home_contact_cta_title')) :?>   
                    <h3><?php the_field('home_contact_cta_title'); ?></h3>
                <?php endif; ?>
                <?php if(get_field('home_contact_cta_subtitle')) :?>   
                    <p><?php the_field('home_contact_cta_subtitle'); ?></p>
                <?php endif; ?>
                <div class="padding-helper"></div>

                <?php if(get_field('home_contact_cta_button_name')) :?>   
                    <a href="#footer" class="btn btn-white"><?php the_field('home_contact_cta_button_name'); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<!-- Home testimonials -->
<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <?php if(get_field('home_testimonails_title')) :?>
                    <h2><?php the_field('home_testimonails_title') ?></h2>
                <?php endif; ?>
                <?php if(get_field('home_testimonails_subtitle')) :?>
                    <p><?php the_field('home_testimonails_subtitle') ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="owl-carousel owl-testimonials">
        <?php 
            $args = array(  
                'post_type' => 'testimonials',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'DESC',
            ); 
                            
                $the_query = new WP_Query( $args );
            ?>
                <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                        <div class="item">
                            <div class="content">
                                <div class="image">
                                    <?php echo get_the_post_thumbnail(''); ?>
                                </div>
                                <div class="info">
                                    <h4>
                                        <?php the_title(); ?>
                                    
                                    </h4>
                                    <p><?php echo wp_trim_words(get_the_content(), 20, '...') ?></p>
                                </div>
                            </div>
                        </div>
 
                    <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>

    </div>
</section>


 

<?php get_footer(); ?>