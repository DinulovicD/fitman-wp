<?php
/**
 * Template Name: About
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package itc_starter
 */

get_header(); ?>

<!-- Header image -->
<section id="header-image" style="background-image: url('<?php the_field('header_image'); ?>'); ">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-image-content">
                    <h2><?php single_post_title(); ?></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                    endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>




 

<?php get_footer(); ?>