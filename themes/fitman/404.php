<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package itc_starter
 */

get_header(); ?>

  <!-- Header image -->
  <section id="header-image" style="background-image: url('/wp-content/themes/fitman/assets/images/bg_image.png'); ">
      <div class="container">
          <div class="row">
              <div class="col"> 
                  <div class="header-image-content">
                      <h2><?php single_post_title(); ?></h2>
                  </div>
              </div>
          </div>
      </div>
  </section>

 <section>
   <div class="container">
     <div class="row">
       <div class="col">
        <div id="primary" class="primary primary--404">
          <main id="main" class="main">

            <article class="entry entry--404">

              <div class="entry__content"> 
                <h1>Oops! That page can’t be found.</h1>
                <p>It looks like nothing was found at this location.</p>
                <a href="<?php echo get_site_url(); ?>" class="btn btn-red">Home</a>
              </div>

            </article><!-- .entry-404 -->

          </main><!-- #main -->
        </div><!-- #primary -->
       </div>
     </div>
   </div>
 </section>

<?php
get_footer();
