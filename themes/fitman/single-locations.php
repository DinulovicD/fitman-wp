<?php
/**
 * The template for displaying locations single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package itc_starter
 */

get_header(); ?>
<!-- Header image -->
<section id="header-image" style="background-image: url('<?php the_field('header_image'); ?>'); ">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="header-image-content">
                    <h2><?php single_post_title(); ?></h2>
                    <?php if(get_field('address')) : ?>
                        <p><?php the_field('address'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="primary" class="primary primary--single locations">

    <main id="main" class="main">

        <?php while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class('entry entry--post'); ?>>

                <!-- <div class="entry__content wysiwyg"> -->
                    <div class="container">
                    <div class="row"> 
                        <div class="col-lg-8 col-md-12">
                            <?php the_content(); ?>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="locations-info">
                                <h3>
                                <?php single_post_title(); ?>
                                
                                </h3>
                                <ul class="location-content-social">
                                    <?php if(get_field('facebook')) :?> 

                                    <li><a href="<?php the_field('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i> </a></li>

                                    <?php endif; ?>



                                    <?php if(get_field('instagram')) :?>

                                    <li><a href="<?php the_field('instagram'); ?>" target="_blank"><i class="fa fa-instagram"></i> </a></li>

                                    <?php endif; ?>

                                </ul>
                                <div class="work-hours">
                                    <?php if( have_rows('work_hours') ): while ( have_rows('work_hours') ) : the_row(); ?>

                                        <div class="time-content">
                                            <?php if(get_sub_field('day')) :?>
                                                <span class="day"><?php the_sub_field('day') ?></span>
                                            <?php endif; ?>
                                            <?php if(get_sub_field('time')) :?>
                                                <span class="hours"><?php the_sub_field('time') ?></span>
                                            <?php endif; ?>
                                        </div>

                                    <?php  endwhile; endif; ?>
                                </div>
                            </div>
                            <script type="text/javascript">
                                var adrese = Array();
                            </script>
 
                                    <script type="text/javascript">
                                        adrese.push('<?php the_field('address') ?>');
                                    </script>

                            <div id="map"></div>
                        </div>
                    </div>
                    </div>
                <!-- </div> -->

            </article>

        <?php endwhile; ?>

    </main><!-- #main -->

</div><!-- #primary -->



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4svvzT4BhQQpFQhaHkTw3fTSbP1E1a8c&callback=initMap" async defer></script>

<script type="text/javascript">

var geocoder;
var map;
var image = '/wp-content/themes/fitman/assets/images/map-marker.png';
function initMap() {

    geocoder = new google.maps.Geocoder();
    latlng = new google.maps.LatLng(38.7954759, -76.7110324);

    mapOptions = {
        zoom: 6, 
        center: latlng,
        styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
}



map = new google.maps.Map(document.getElementById('map'), mapOptions);
adrese.map(function(addr){ 
    codeAddress(addr);
});

}
</script>

<script type="text/javascript">

function codeAddress(address) {
		if(address) {
			geocoder.geocode( { 'address': address + ', Virginia'}, function(results, status) {
				if (status == 'OK') {
					var marker = new google.maps.Marker({
						map: map,
						position: results[0].geometry.location,
						animation: google.maps.Animation.DROP,
						icon: image
					});
				} else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
					setTimeout(function() {
						codeAddress(address);
					}, 250 );
				} else {
					//alert('Geocode was not successful for the following reason: ' + status);
				}
			});
		}
	}

	setTimeout(function(){
		adrese.map(function(addr){
			codeAddress(addr);
		});
	}, 1000);

	

</script>

<?php get_footer();
