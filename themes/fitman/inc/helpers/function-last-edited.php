<?php
/**
 * Cache hooks and functions
 *
 * @package itc_starter
 */

/**
 * Get last-edited timestamp
 *
 * @global array $itc_starter_timestamps cached timestamp values
 *
 * @param string $asset ID of asset type
 *
 * @return int UNIX timestamp
 */
function itc_starter_last_edited($asset = 'css') {

  global $itc_starter_timestamps;

  // save timestamps to cache in global variable for this request
  if (empty($itc_starter_timestamps)) {

    $filepath = get_template_directory() . '/assets/last-edited.json';

    if (file_exists($filepath)) {
      $json = file_get_contents($filepath);
      $itc_starter_timestamps = json_decode($json, true);
    }

  }

  // use cached value from global variable
  if (isset($itc_starter_timestamps[$asset])) {
    return absint($itc_starter_timestamps[$asset]);
  }

  return 0;

}
