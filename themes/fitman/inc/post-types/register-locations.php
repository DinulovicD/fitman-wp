<?php

$locations = new CPT( array(
    'post_type_name' => 'locations',
    'singular'       => __('Locations', 'tht'),
    'plural'         => __('Locations', 'tht'),
    'slug'           => 'locations'
),
	array(
    'supports'  => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'comments' ),
    'menu_icon' => 'dashicons-location'
));

$locations -> register_taxonomy( array(
    'taxonomy_name' => 'locations_tags', 
    'singular'      => __('Locations Tag', 'tht'),
    'plural'        => __('Locations Tags', 'tht'),
    'slug'          => 'locations-tag'
));