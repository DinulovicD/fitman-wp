<?php

$testimonials = new CPT( array(
    'post_type_name' => 'testimonials',
    'singular'       => __('Testimonials', 'tht'),
    'plural'         => __('Testimonials', 'tht'),
    'slug'           => 'testimonials'
),
	array(
    'supports'  => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'comments' ),
    'menu_icon' => 'dashicons-testimonial'
));

$testimonials -> register_taxonomy( array(
    'taxonomy_name' => 'testimonials_tags',
    'singular'      => __('Testimonials Tag', 'tht'),
    'plural'        => __('Testimonials Tags', 'tht'),
    'slug'          => 'testimonials-tag'
));