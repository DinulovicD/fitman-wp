/* ==========================================================================
  main.js
========================================================================== */

/**
 * Navigation
 */
aucor_navigation(document.getElementById('primary-navigation'), {
  desktop_min_width: 890, // min width in pixels
  menu_toggle: '#menu-toggle' // selector for toggle
});

/**
 * Responsive videos
 */
fitvids();

/**
 * Polyfill object-fit for lazyloaded
 */
if (typeof objectFitPolyfill === "function") {
  document.addEventListener('lazybeforeunveil', function (e) {

    // current <img> element
    var el = e.target;

    objectFitPolyfill();
    el.addEventListener('load', function () {
      objectFitPolyfill();
    });

  });
}


$(document).ready(function () {
  /*
   * Owl locations
   */
  var locationOwl = $('.owl-locations').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    // autoplay: true,
    // autoplaySpeed: 2000,
    // dots: true,
    // nav: true, 
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      1000: {
        items: 3
      }
    }
  });

  /*
   * Owl testimonials
   */
  $('.owl-testimonials').owlCarousel({
    loop: true,
    margin: 0,
    responsiveClass: true,
    // autoplay: true,
    // autoplaySpeed: 2000,
    dots: true,
    nav: true,
    navText: ["<i class='fitman-left'></i>", "<i class='fitman-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 2.2,
        nav: true,
        // loop: false
      }
    }
  });

  /*
   * Scroll up
   */
  $('.scrollup').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 600);
    return false;
  });

  /*
   * Link scroll animation
   */
  $(".primary-navigation__items>li>span>a, .home-content a, #cta a").click(function () {

    $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top - 114,
    }, 600);
  });
  /*
   * Slick
   */
  var slick = $('.slick-carousel');
  slick.slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 1500,
    speed: 500,
    vertical: true, 
    verticalSwiping: true,
    slidesToShow: 2,
    dots: false,
    infinite: true, 
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2, 
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  /*
   * Add affix class on scroll header
   */
  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    // console.log(scroll);

    if (scroll >= 100) {
      $('header.site-header').addClass('affix');
    } else {
      $('header.site-header').removeClass('affix');
    }

    // Do something
  });

});
